import React, { useState, useEffect, useCallback } from "react";
import useToken from "@galvanize-inc/jwtdown-for-react";

function ListAllCocktails({ id }) {
  const [cocktails, setCocktails] = useState([]);
  const { fetchWithToken, token } = useToken();
  const fetchCocktails = useCallback(
    async (accountId) => {
      try {
        const data = await fetchWithToken(
          `${process.env.REACT_APP_API_HOST}/accounts/${accountId}/cocktails/`
        );
        setCocktails(data);
      } catch (error) {
        console.error("Error fetching cocktails, yo:", error);
      }
    },
    [fetchWithToken]
  );

  useEffect(() => {
    if (token && cocktails.length === 0) {
      fetchCocktails(id);
    }
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <>
      <h1>Your Created Cocktails </h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Name</th>
            <th>Picture</th>
            <th>Description</th>
          </tr>
        </thead>
        <tbody>
          {cocktails.map((cocktail) => (
            <tr key={cocktail.id}>
              <td>{cocktail.cocktail_name}</td>
              <td>
                <img
                  src={cocktail.picture_url}
                  alt={cocktail.cocktail_name}
                  style={{ width: "200px", height: "auto" }}
                />
              </td>
              <td>{cocktail.description}</td>
              <td>{cocktail.username}</td>
              <td>{cocktail.name}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </>
  );
}

export default ListAllCocktails;
