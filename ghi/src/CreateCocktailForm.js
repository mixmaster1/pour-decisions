import React, { useEffect, useState } from "react";
import useToken from "@galvanize-inc/jwtdown-for-react";
import { useNavigate } from "react-router-dom";

function CreateCocktailForm() {
  const [flavor_profiles, setFlavorProfiles] = useState([]);
  const [cocktail_name, setCocktailName] = useState("");
  const [flavor_profile, setFlavorProfile] = useState("");
  const [picture_url, setPictureUrl] = useState("");
  const [description, setDescription] = useState("");
  const { token } = useToken();
  const navigate = useNavigate();
  const fetchData = async () => {
    const url = `${process.env.REACT_APP_API_HOST}/flavors`;
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setFlavorProfiles(data);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  const handleCocktailNameChange = (event) => {
    const value = event.target.value;
    setCocktailName(value);
  };

  const handleFlavorProfileChange = (event) => {
    const value = event.target.value;
    setFlavorProfile(value);
  };

  const handlePictureUrlChange = (event) => {
    const value = event.target.value;
    setPictureUrl(value);
  };

  const handleDescriptionChange = (event) => {
    const value = event.target.value;
    setDescription(value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {};
    data.cocktail_name = cocktail_name;
    data.flavor_profile = flavor_profile;
    data.picture_url = picture_url;
    data.description = description;


    const cocktailUrl = `${process.env.REACT_APP_API_HOST}/cocktails`;
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
    };
    const response = await fetch(cocktailUrl, fetchConfig);
    if (response.ok) {
      const { id } = await response.json();
      navigate(`/cocktails/${id}/ingredients`);
    }
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add a New Cocktail</h1>
          <form onSubmit={handleSubmit} id="newCocktailForm">
            <div className="form-floating mb-3">
              <input
                onChange={handleCocktailNameChange}
                value={cocktail_name}
                placeholder="Cocktail Name"
                required
                type="text"
                id="cocktail_name"
                name="cocktail_name"
                className="form-control"
              />
              <label htmlFor="cocktail_name">Cocktail Name</label>
            </div>
            <div className="mb-3">
              <label htmlFor="flavor_profile" className="form-label">
                Flavor Profile
              </label>
              <select
                onChange={handleFlavorProfileChange}
                value={flavor_profile}
                required
                id="flavor_profile_id"
                name="flavor_profile_id"
                className="form-select"
              >
                <option value="">Choose a Flavor Profile</option>
                {flavor_profiles.map((flavor_profile) => {
                  return (
                    <option key={flavor_profile.id} value={flavor_profile.id}>
                      {flavor_profile.name}
                    </option>
                  );
                })}
              </select>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handlePictureUrlChange}
                value={picture_url}
                placeholder="Picture URL"
                required
                type="url"
                id="picture_url"
                name="picture_url"
                className="form-control"
              />
              <label htmlFor="picture_url">Picture URL</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleDescriptionChange}
                value={description}
                placeholder="Describe your cocktail"
                required
                type="text"
                id="description"
                name="description"
                className="form-control"
              />
              <label htmlFor="description">Description</label>
            </div>
            <button className="btn btn-success">
              Move onto adding your ingredients!
            </button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default CreateCocktailForm;
