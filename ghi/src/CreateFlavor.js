import React, { useState } from "react";

function CreateFlavorForm() {
  const [name, setName] = useState("");

  const handleNameChange = (event) => {
    setName(event.target.value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {};
    data.name = name;

    const flavorUrl = `${process.env.REACT_APP_API_HOST}/flavors`;
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(flavorUrl, fetchConfig);
    if (response.ok) {
      const newFlavor = await response.json();
      console.log(newFlavor)
      setName("");
    }
  };

  return (
    <>
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a flavor</h1>
            <form onSubmit={handleSubmit} id="create-flavor-form">
              <div className="form-floating mb-3">
                <input
                  onChange={handleNameChange}
                  value={name}
                  placeholder="Flavor..."
                  required
                  type="text"
                  id="name"
                  name="name"
                  className="form-control"
                />
                <label htmlFor="name">Flavor...</label>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    </>
  );
}

export default CreateFlavorForm;
