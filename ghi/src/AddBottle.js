import { useState } from "react";
import useToken from "@galvanize-inc/jwtdown-for-react";
import { NavLink } from "react-router-dom";

const CreateLiquor = () => {
  const [name, setName] = useState("");
  const [type, setType] = useState("");
  const [description, setDescription] = useState("");
  const [picture_url, setPictureUrl] = useState("");
  const { token } = useToken();

  const handleLiquorCreation = async (e) => {
    e.preventDefault();

    const liquorData = {
      brand: name,
      type: type,
      description: description,
      picture_url: picture_url,
    };

    const response = await fetch(`${process.env.REACT_APP_API_HOST}/spirits`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify(liquorData),
    });

    if (!response.ok) {
      const errorData = await response.json();
      console.error("Error:", errorData.message);
    } else {
      console.log("Liquor Added all up in here!");
      e.target.reset();
    }
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create Liquor</h1>
          <form onSubmit={(e) => handleLiquorCreation(e)}>
            <div className="form-floating mb-3">
              <input
                required
                type="text"
                name="name"
                className="form-control"
                onChange={(e) => {
                  setName(e.target.value);
                }}
              />
              <label className="form-label">Bottle Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                required
                type="text"
                name="type"
                className="form-control"
                onChange={(e) => {
                  setType(e.target.value);
                }}
              />
              <label className="form-label">Type</label>
            </div>
            <div className="form-floating mb-3">
              <input
                required
                type="text"
                name="description"
                className="form-control"
                onChange={(e) => {
                  setDescription(e.target.value);
                }}
              />
              <label className="form-label">Description</label>
            </div>
            <div className="form-floating mb-3">
              <input
                type="text"
                name="pictureUrl"
                className="form-control"
                onChange={(e) => {
                  setPictureUrl(e.target.value);
                }}
              />
              <label className="form-label">Picture or Label</label>
            </div>
            <button className="btn btn-primary">Add Bottle</button>
            <NavLink
              className="nav-link active"
              aria-current="page"
              to="/cabinet"
            >
              <button className="btn btn-primary">Back to My Cabinet</button>
            </NavLink>
          </form>
        </div>
      </div>
    </div>
  );
};

export default CreateLiquor;
