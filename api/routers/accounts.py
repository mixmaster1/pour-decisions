from fastapi import (
    Depends,
    HTTPException,
    status,
    Response,
    APIRouter,
    Request,
    Path,
    Body,
)
from pydantic import BaseModel
from authenticator import authenticator
from jwtdown_fastapi.authentication import Token
from queries.accounts import (
    AccountIn,
    AccountOut,
    AccountQueries,
    DuplicateAccountError,
)


class AccountForm(BaseModel):
    username: str
    password: str


class AccountToken(Token):
    account: AccountOut


class HttpError(BaseModel):
    detail: str


router = APIRouter()


@router.get("/token", response_model=AccountToken | None)
async def get_token(
    request: Request,
    account: AccountOut = Depends(authenticator.try_get_current_account_data),
):
    if account and authenticator.cookie_name in request.cookies:
        return {
            "access_token": request.cookies[authenticator.cookie_name],
            "type": "Bearer",
            "account": account,
        }


@router.post("/api/accounts", response_model=AccountToken | HttpError)
async def create_account(
    info: AccountIn,
    request: Request,
    response: Response,
    accounts: AccountQueries = Depends(),
):
    hashed_password = authenticator.hash_password(info.password)
    try:
        account = accounts.create(info, hashed_password)
    except DuplicateAccountError:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Cannot create account",
        )
    print(account)
    form = AccountForm(username=info.username, password=info.password)
    token = await authenticator.login(response, request, form, accounts)
    return AccountToken(account=account, **token.dict())


@router.get("/api/accounts/{username}", response_model=AccountOut | HttpError)
async def get_user_account(
    username: str,
    accounts: AccountQueries = Depends(),
):
    user_data = accounts.get_user(username)
    return user_data


@router.put(
    "/api/accounts/{username}",
    response_model=AccountOut,
)
async def update_user(
    username: str = Path(..., description="Account username"),
    user_data: AccountIn = Body(..., description="Updated user data"),
    accounts: AccountQueries = Depends(),
):
    update_user = accounts.update_user(username, user_data)
    if update_user is None:
        raise HTTPException(status_code=404, detail="User not found")
    return update_user
