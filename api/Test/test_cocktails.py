from fastapi.testclient import TestClient
from main import app
from queries.cocktails import CocktailRepository


client = TestClient(app)


class EmptyCocktailRepository:
    def get_all(self):
        result = [
            {
                "id": 5,
                "cocktail_name": "Old Fashioned",
                "picture_url": "string",
                "description": "string",
                "username": "string",
                "name": "smokey"
            }
        ]
        return result


def test_get_all_cocktails():

    app.dependency_overrides[CocktailRepository] = EmptyCocktailRepository
    response = client.get("/cocktails")

    expected = [
        {
            "id": 5,
            "cocktail_name": "Old Fashioned",
            "picture_url": "string",
            "description": "string",
            "username": "string",
            "name": "smokey"
        }
    ]

    app.dependency_overrides = {}

    assert response.status_code == 200
    assert response.json() == expected
