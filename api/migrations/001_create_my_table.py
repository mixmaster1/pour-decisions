steps = [
    [
        ## Create a table
        """
        CREATE TABLE users (
            id SERIAL PRIMARY KEY UNIQUE,
            username VARCHAR(255) NOT NULL,
            hashed_password VARCHAR(255) NOT NULL,
            email VARCHAR(255) NOT NULL UNIQUE,
            birthday DATE
        );
        """,
        ### Drop the table
        """
        DROP TABLE users;
        """,
    ],
    [
        """
        CREATE TABLE spirits (
            id SERIAL PRIMARY KEY UNIQUE,
            type VARCHAR(255) NOT NULL,
            brand VARCHAR(255) NOT NULL,
            picture_url VARCHAR(255),
            description VARCHAR(255),
            owner INTEGER NOT NULL REFERENCES users("id") ON DELETE CASCADE
        );
        """,
        ### Drop the table
        """
        DROP TABLE spirits;
        """,
    ],
    [
        """
        CREATE TABLE flavor_profile (
            id SERIAL PRIMARY KEY UNIQUE,
            name VARCHAR(255) NOT NULL
        );
        """,
        ### Drop the table
        """
        DROP TABLE flavor_profile;
        """,
    ],
    [
        """
        CREATE TABLE cocktails (
            id SERIAL PRIMARY KEY UNIQUE,
            cocktail_name VARCHAR(255) NOT NULL,
            flavor_profile INTEGER REFERENCES flavor_profile("id"),
            picture_url VARCHAR(255),
            description VARCHAR(255) NOT NULL,
            mixologist INTEGER NOT NULL REFERENCES users("id")
        );
        """,
        ### Drop the table
        """
        DROP TABLE cocktails;
        """,
    ],
    [
        """
        CREATE TABLE ingredients (
            id SERIAL PRIMARY KEY UNIQUE,
            name VARCHAR(255) NOT NULL,
            is_spirit BOOLEAN NOT NULL,
            quantity VARCHAR(255) NOT NULL,
            cocktail_id INTEGER  NOT NULL REFERENCES cocktails("id") ON DELETE CASCADE
        );
        """,
        ### Drop the table
        """
        DROP TABLE ingredients;
        """,
    ],
]
