- Week 5
  This week Zach and I took on the deployment undertaking, it wasnt so bad as long as you follow instructions to a T. I have spent a lot of time solving pipeline issues, mainting our groups formatting goals and trying to keep a cohesive codebase as if it was written by one developer. We have spent the rest of the week bugfixing our deployed site and are crossing Ts and dotting i's for our presentation on Friday.

- Week 4
  This week I delved deeper into the frontend and tasked myself with creating an explore page where a logged in user can see all of the cocktails other users have made in an instagram explore page style. I used bootstrap to create placard views of all the cocktails that contain hyperlinks to the details of each cocktail with their respective recipe and directions. I also wanted to show the username of the mixologist that created said cocktail so I took care of that. I also made myself available to my team for assistance if they needed any help with what they were working on.

- Week 3
  This week I put the final touches i.e backend Auth on my spirits endpoints. We as a group wrapped up the rest of our backend and began on the front end journey. After some discussing we landed on using bootstrap for our front end library because we were not all comfortable with frontend work and we have used it before, plus we will be able to go deeper and deeper with customization once we reach stretch goals week. I completed the front end for users, Create Account, Login, Logout, and implemented front end auth along with it.

- Week 2 (After Break)
  This week I tied the bow on the users endpoints and began implementing back end auth for them. I then went on to write all of the endpoints for our spirits table, Get, Post, Delete. Having completed those endpoints first, rather than moving onto front end stuff I stayed back and helped my team finish the rest of the backend.

- Week 1
  This week I focused a lot on ideation, wireframing, mapping out some backend endpoints. Towards the end of the week we as a group began coding, I sat in the passsengers seat to get out data tables started, we completed the users tables. Then I sat in the drivers seat and wrote out the back end endpoints i.e queries and routers for the users.
