## October 27, 2023

Today, I worked on:

- Doing the final checking of the project checklist

I went through the checklist of the project and mostly
cleaned the dead code and formatted the code to make it
cleaner.

## October 26, 2023

Today, I worked on:

- Unit test and Readme.md

I worked on unit test of my create flavor backend and
added the steps of handling errors as well as writing
readme.md and related documents to clearly show the
functionalities and logic of our project.

## October 25, 2023

Today, I worked on:

- Hard coding common flavors into the database

I worked on hard coding common cocktail flavors into
database and try to remove the typed-in flavor from
the form after the user click on the create flavor
button so that indicates the user has sccuessfully
created a new flavor.

Today, I checked the different ways of inserting flavors
into the whole data table by creating an inserting function
or inserting data directly into the database. I tried out
both methods so that I know both ways of doing it now.

## October 24, 2023

Today, I worked on:

- Finalizing readme.md, apis.md, datamodel.md, ghi.md,
  and integration.md

I worked on documentation to precicely descripe each
specific features and their functions so that whenver
people checkout out documentations they would be able
to see clearly how does each feature work and what is
their purpose.

Today, I learned to do documentation in concise wording.

## October 23, 2023

Today, I worked on:

- Finalizing unit test for flavor create backend

I worked on the unit test for my create flavor form by
writing functions to check if the delivered result
matches the expected result, which turned out to be true.

Today, I encountered migrations behind issue when trying
to pass the create flavor unit test. I learned that when
that issue happened, I need to delete the volume and the
containers and rebuild them so that migrations would match.

## October 19, 2023

Today, I worked on:

- Unit test for flavor create backend

My team and I revised out table designing by
changing the thought of deleting ingredients table
to keeping the ingredients table and putting in spirits
and non-alcholic ingredients inside this table, and
meanwhile adding boolean field for each ingreditents
to differentiate between the alcholic ones and non-alcholic
ones. In this way, we would be able to both reduce the
workload and set up a better foundation for further
development in the future.

Today, I checked the different ways of inserting into the
whole data table and inserting data line by line. I tried
out both methods in our data table so that I know both ways
of doing it now.

## October 18, 2023

Today, I worked on:

- Unit test for front-end component: the create flavor
  form

I mistakenly thought we will need to write unit test for
front-end component so that I wrote unit test for my create
flavor form and made it passed the test.

Today, even though I did the non-requiredunit test for
front-end component, I learned the way to unit test front-end,
ensured my front-end component worked well and I will
definitely use that in the future.

## October 17, 2023

Today, I worked on:

- Front-end flavor list json file

I worked on the front-end flavor list json file, which is
for users to see all the flavors created by the website users
and orginally hard coded into the database. And the user can
use these flavors to label their own cocktail recipes.

## October 16, 2023

Today, I worked on:

- Front-end Flavor create form json file

I worked on the flavor creating form json file for users to
add their original, creative flavors besides the common ones,
which would be hard coded into the database.

## October 13, 2023

Today, I worked on:

- API endpoints for getting, deleting flavors

I worked on the endpoints for getting all the
flavors and delete a particular flavor based on
flavor id.

## October 12, 2023

Today, I worked on:

- API endpoints for creating flavors

I worked on the endpoint for creating flavors
that could enable users to create their own
original flavors into the database.

## October 11, 2023

Today, I worked on:

- Finalizing tables of the website with my team

My team and I revised out table designing by
changing the thought of deleting ingredients table
to keeping the ingredients table and putting in spirits
and non-alcholic ingredients inside this table, and
meanwhile adding boolean field for each ingreditents
to differentiate between the alcholic ones and non-alcholic
ones. In this way, we would be able to both reduce the
workload and set up a better foundation for further
development in the future.

Today, I checked the different ways of inserting into the
whole data table and inserting data line by line. I tried
out both methods in our data table so that I know both ways
of doing it now.

## October 10, 2023

Today, I worked on:

- Writing tables the website need with my team

My team and I worked on the tables for the website
and I personally contributed to create the table for
flavor profile of cocktails recipes. I hardcoded the
11 flavors into the profiles, which will be options
for the dropdown list used by users when creating reccipes
and trying to categorize their flavors.

Today, I learned that when we want to incorporate data or
info into the table, we can use the syntax INSERT INTO
tablename. I used this method to insert 11 flavors of
cocktails into the flavor profile tables, which will be
used as dropdown list options in future.

## October 9, 2023

Today, I worked on:

- Testing API endpoints of Autentication of the
  website with my team

My team and I tested the API endpoints for the
autentication of users and accounts, which we
first thought we have problem signing in to the
website after creating an account. However, it turned
out that the 422 code just showed the possible error
position and syntax and the endpoints for the autentication
worked successfully when both creating account and
signing in to the website.

Today, I tested the authentication API endpoints and
I found out that the 422 code does not mean there is
error inside out endpoints but rather it is an example
error message indicating where error would show up.

## September 28, 2023

Today, I worked on:

- Writing API endpoints of Autentication of the
  website with my team

My team and I wrote the API endpoints for the
autentication of users and accounts, which we have
designed a few days ago during the webframe design.
Meanwhile, we fix the user table by changing the
birthday from could be null to not null since our
website needs age verification.

Today, I wrote the issue for writing authentication
API endpoints and I found out that writing issues
step by step is so important regarding organization.
I am going to be using this a lot!

## September 27, 2023

Today, I worked on:

- Writing Postgre table of users and account with
  my team

My team and I wrote the SQL for users and accounts,
which we designed yesterday. We wrote journals and
worked on the git as a group and figured out how to
commit changes and push them to git remote for each
branch created by each issue.

Today, I found that we would work on a particular git
branch created by each issue we created and push any
changes to that branch. I am going to be
using this a lot!

## September 26, 2023

Today, I worked on:

- Designing the user Postgre table with my team.

My team and I wrote the structure of our SQL table
in terms of account and users. We restudied how to
create database, table and insert data into table.

Today, I found the . I am going to be
using this a lot!

## September 25, 2023

Today, I worked on:

- Figuring out the API endpoints of Pour Decisions
  website with other team members.

My team and I wrote some endpoints for CRUD endpoints
for spirit, cocktail recipes and users. We discussed
about the priorities of the website's functionalities
and make the MVP and stretch goals more clear and specfic.

Today, I learned how to use AI picture generator to create
cocktail images. I am going to be using this a lot in our
cocktail exploration page!
